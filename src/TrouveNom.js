import React, { Component } from 'react';
import axios from 'axios';

export default class TrouveNom extends Component {

  constructor(props){
    super(props);
  }

  render() {
    return (
      <form onSubmit= {this.trouveNom.bind(this)}>
        <input type="text" placeholder="mettre le nom" ref="createInput"/>
        <button>Trouve</button>
      </form>
    );
  }

  trouveNom(event) {
    event.preventDefault();

    axios.get("http://localhost:8070/nom/" + this.refs.createInput.value)
        .then(response => {

          /*msg = response.data.message,
          name = response.data.nom,
          emiel = response.data.email,
          ident = response.data.id
          this.props.trouveNom(msg, name, emiel, ident);
          */
          this.props.trouveNom( response.data.message, response.data.nom, response.data.email, response.data.id);
          /*
          this.setState({
            message:response.data.message,
            nom: response.data.nom,
            email: response.data.email,
            id: response.data.id
          })
          */
          console.log(this.state);
        })
        .catch((error) => {
          console.log("error",error)
        })

    //this.props.trouveNom(msg, name, emiel, ident);
    //this.props.trouveNom( response.data.message, response.data.nom, response.data.email, response.data.id);

    //Arrive pas à refresh pour que les data soit afficher - Trouver Finalement
  }

}
