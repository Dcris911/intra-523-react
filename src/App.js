import React, { Component } from 'react';

import Message from './Message.js'
import TrouveNom from './TrouveNom.js'
import axios from 'axios';
import './App.css';

class App extends Component {

  constructor(props) {
    super(props)

    this.state = {
      message: "",
      nom: "",
      email: "",
      id: ""
    }
  }

  componentDidMount(){
          axios.get("http://localhost:8070/index/")
          .then(response => {

            this.setState({
              message:response.data.message
            })
            console.log(this.state);
          })

          .catch((error) => {
            console.log("error",error)
          })
      }

  render() {
    return (
      <div>
        <Message message={this.state.message}></Message>
        <TrouveNom trouveNom={this.trouveNom.bind(this)}/>
        <p>Nom: {this.state.nom}</p>
        <p>Email: {this.state.email}</p>
        <p>Id: {this.state.id}</p>
      </div>
    );
  }

  trouveNom(message, nom, email, id) {
    //this.setState(this.state);
    this.setState({
      nom: nom,
      id: id,
      email: email,
      message: message
    })
    /*console.log(nom);
    console.log(email);
    console.log(id);*/
  }
}

export default App;
